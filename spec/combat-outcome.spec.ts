import { CombatOutcome } from '../src/combat-outcome';
import { ICombatOutcome } from '../src/contracts/combat-outcome';
import { ILocalStorageDatabaseManipulator } from '../src/contracts/data/localStorageDatabaseManipulator';
import { IPlayerStatsUpdater } from '../src/contracts/dom-manipulator/player-stats-updater';
import { INPC, IPlayer } from '../src/contracts/models';
import { IDomManipulator } from '../src/contracts/models/DOMManipulation/IDomManipulator';
import { NPC, Player } from '../src/models';
import { ElementsStats } from '../src/models/elements-stats-model';
import { LocalStorageDatabaseManipulator } from './../src/data/localStorageDatabaseManipulator';

describe('CombatOutcome', () => {
  let playerStatsUpdatorMock: jest.Mock<IPlayerStatsUpdater>;
  let domMock: jest.Mock<IDomManipulator>;
  let dbManipulatorMock: jest.Mock<ILocalStorageDatabaseManipulator>;

  beforeEach(() => {
    dbManipulatorMock = jest
      .fn<ILocalStorageDatabaseManipulator>()
      // tslint:disable-next-line:no-empty
      .mockImplementation(() => ({}));

    dbManipulatorMock = jest
      .fn<ILocalStorageDatabaseManipulator>()
      // tslint:disable-next-line:no-empty
      .mockImplementation(() => ({
        savePlayerData: jest.fn(),
        savePlayersPassedLevels: jest.fn(),
        savePlayerInventory: jest.fn()
      }));

    domMock = jest.fn<IDomManipulator>().mockImplementation(() => ({
      logToConsole: jest.fn(),
      showStartGameButton: jest.fn(),
      playSound: jest.fn(),
      hideLoadButton: jest.fn()
    }));

    playerStatsUpdatorMock = jest
      .fn<IPlayerStatsUpdater>()
      // tslint:disable-next-line:no-empty
      .mockImplementation(() => { });
  });

  describe('onDefeat method should', () => {
    it('call all dom manipulation methods', () => {
      // Arrange
      const dom: IDomManipulator = new domMock();
      const db: ILocalStorageDatabaseManipulator = new dbManipulatorMock();
      const playerStatsUpdator: IPlayerStatsUpdater = new playerStatsUpdatorMock();

      const combatOutcome: ICombatOutcome = new CombatOutcome(
        dom,
        db,
        playerStatsUpdator
      );

      const player: IPlayer = new Player(
        '',
        1,
        1,
        new ElementsStats(0, 0, 0),
        [],
        1,
        1
      );
      const npc: INPC = new NPC('', 0, 1, new ElementsStats(0, 0, 0), [], '');

      const spyLogger: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');
      const spySoundPlayer: jest.SpyInstance = jest.spyOn(dom, 'playSound');
      const spyStartButtonShower: jest.SpyInstance = jest.spyOn(
        dom,
        'showStartGameButton'
      );

      // Act
      combatOutcome.onDefeat(player, npc);

      // Assert
      expect(spyLogger).toHaveBeenCalled();
      expect(spySoundPlayer).toHaveBeenCalled();
      expect(spyStartButtonShower).toHaveBeenCalled();
    });
  });

  // describe('On win:', () => {
  //   it('methods for saving player`s progress,boss level & imventory to localStorage must be called', () => {
  //     const dom: IDomManipulator = new domMock();
  //     const db: ILocalStorageDatabaseManipulator = new dbManipulatorMock();
  //     const playerStatsUpdator: IPlayerStatsUpdater = new playerStatsUpdatorMock();

  //     const combatOutcome: ICombatOutcome = new CombatOutcome(
  //       dom,
  //       db,
  //       playerStatsUpdator
  //     );

  //     const spyPlayersaver: jest.SpyInstance = jest.spyOn(db, 'savePlayerData');
  //     const spySavePlayersPassedLevels: jest.SpyInstance = jest.spyOn(
  //       db,
  //       'savePlayersPassedLevels'
  //     );
  //     const spysavePlayerInventory: jest.SpyInstance = jest.spyOn(
  //       db,
  //       'savePlayerInventory'
  //     );

  //     const player: IPlayer = new Player(
  //       '',
  //       1,
  //       1,
  //       new ElementsStats(0, 0, 0),
  //       [],
  //       1,
  //       1
  //     );
  //     const npc: INPC = new NPC('', 0, 1, new ElementsStats(0, 0, 0), [], '');

  //     combatOutcome.onWin(player, npc);

  //     expect(spyPlayersaver).toHaveBeenCalled();
  //     expect(spySavePlayersPassedLevels).toHaveBeenCalled();
  //     expect(spysavePlayerInventory).toHaveBeenCalled();
  //   });
  // });
});
