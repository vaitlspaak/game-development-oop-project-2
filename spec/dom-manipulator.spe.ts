/* import { INPC, IPlayer } from '../src/contracts/models';
import { IDomManipulator } from '../src/contracts/models/DOMManipulation/IDomManipulator';
import { NPC, Player } from '../src/models';
import { DOMManipulator } from '../src/models/DOMManipulation/domManipulator';
import { ElementsStats } from '../src/models/elements-stats-model';

describe('DOMManipulator', () => {
    describe('updateAllBars method should', () => {
        it('update player\'s hp + mana and hp of the boss', () => {
            // Arrange
            const player: IPlayer = new Player('', 1, 1, new ElementsStats(0, 0, 0), [], 1, 1);
            const npc: INPC = new NPC('', 0, 1, new ElementsStats(0, 0, 0), [], '');

            const domManipulator: IDomManipulator = new DOMManipulator();

            const spyBossHP: jest.SpyInstance = jest.spyOn(domManipulator, 'updateBossHealthBar');
            const spyPlayerHP: jest.SpyInstance = jest.spyOn(domManipulator, 'updatePlayerHealthBar');
            const spyPlayerMana: jest.SpyInstance = jest.spyOn(domManipulator, 'updatePlayerManaBar');

            // Act
            domManipulator.updateAllBars(player, npc);

            // Assert
            expect(spyBossHP).toHaveBeenCalled();
            expect(spyPlayerHP).toHaveBeenCalled();
            expect(spyPlayerMana).toHaveBeenCalled();
        });
    });
});
 */