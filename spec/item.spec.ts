import { ItemType } from '../src/common';
import { IItem } from '../src/contracts/models';
import { Item } from '../src/models';
import { ElementsStats } from '../src/models/elements-stats-model';

describe('Item', () => {
    describe('upgrade method should', () => {
        it('update item stats if level is more than 1', () => {
            // Arrange
            const item: IItem = new Item('', ItemType.Belt, new ElementsStats(10, 20, 30), 2, 0, 0, '');

            // Act
            item.upgrade();

            // Assert
            expect(item.elementsStats.frostPoints).toBe(15);
            expect(item.elementsStats.firePoints).toBe(30);
            expect(item.elementsStats.naturePoints).toBe(45);
        });

        it('not modify any stats that are initially set to 0', () => {
            const item: IItem = new Item('', ItemType.Belt, new ElementsStats(10, 20, 30), 2, 0, 0, '');

            // Act
            item.upgrade();

            // Assert
            expect(item.bonusHp).toBe(0);
            expect(item.bonusMana).toBe(0);
        });

        it('not modify any stats if item level had been equal to 0', () => {
            const item: IItem = new Item('', ItemType.Belt, new ElementsStats(10, 20, 30), 0, 10, 10, '');

            // Act
            item.upgrade();

            // Assert
            expect(item.elementsStats.frostPoints).toBe(10);
            expect(item.elementsStats.firePoints).toBe(20);
            expect(item.elementsStats.naturePoints).toBe(30);
            expect(item.bonusHp).toBe(10);
            expect(item.bonusMana).toBe(10);
        });
    });
});
