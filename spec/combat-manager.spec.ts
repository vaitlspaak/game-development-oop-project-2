// tslint:disable-next-line
import 'reflect-metadata';
import { BossSpawner } from '../src/boss-spawner';
import { CombatManager } from '../src/combat-manager';
import { CombatOutcome } from '../src/combat-outcome';
import { IBossSpawner } from '../src/contracts/boss-spawner';
import { ICombatManager } from '../src/contracts/combat-manager';
import { ICombatOutcome } from '../src/contracts/combat-outcome';
import { INPC, IPlayer } from '../src/contracts/models';
import { IDomManipulator } from '../src/contracts/models/DOMManipulation/IDomManipulator';
import { INPCAbilityManager } from '../src/contracts/npc-ability-manager';
import { NPC, Player } from '../src/models';
import { DOMManipulator } from '../src/models/DOMManipulation/domManipulator';
import { ElementsStats } from '../src/models/elements-stats-model';
import { NPCAbilityManager } from '../src/npc-ability-manager';

describe('CombatManager', () => {
    let combatOutcomeMock: jest.Mock<ICombatOutcome>;
    let bossSpawnerMock: jest.Mock<IBossSpawner>;
    let domMock: jest.Mock<IDomManipulator>;
    let npcManagerMock: jest.Mock<INPCAbilityManager>;

    beforeEach(() => {
        // tslint:disable-next-line:arrow-return-shorthand
        combatOutcomeMock = jest.fn<ICombatOutcome>().mockImplementation(() => {
            return {
                onWin: jest.fn(),
                onDefeat: jest.fn()
            };
        });

        // tslint:disable-next-line:arrow-return-shorthand
        domMock = jest.fn<IDomManipulator>().mockImplementation(() => {
            return {
                updateAllBars: jest.fn(),
                prepareAbilityButtons: jest.fn(),
                changeBossImage: jest.fn(),
                addAbilityListeners: jest.fn(),
                prepareItemIcons: jest.fn(),
                hideStartGameButton: jest.fn(),
                logToConsole: jest.fn()
            };
        });

        // tslint:disable-next-line:arrow-return-shorthand
        bossSpawnerMock = jest.fn<IBossSpawner>().mockImplementation(() => {
            return {
                // tslint:disable-next-line:arrow-return-shorthand
                getBoss: jest.fn().mockReturnValue(() => {
                    return new NPC('', 0, 1, new ElementsStats(0, 0, 0), [], '');
                })
            };
        });
        // tslint:disable-next-line:no-empty
        npcManagerMock = jest.fn<INPCAbilityManager>().mockImplementation(() => { });
    });

    describe('init method should', () => {
        it('initialize player', () => {
            // Arrange
            const dom: IDomManipulator = new domMock();
            const combatOutcome: ICombatOutcome = new combatOutcomeMock();
            const npcManager: INPCAbilityManager = new npcManagerMock();
            const bspawn: IBossSpawner = new bossSpawnerMock();

            const combatManager: ICombatManager = new CombatManager(combatOutcome, bspawn, dom, npcManager);

            const player: IPlayer = new Player('', 1, 1, new ElementsStats(0, 0, 0), [], 1, 1);

            // Act
            combatManager.init(player);

            // Assert
            expect(combatManager._player).toBeDefined();
            expect(combatManager._player).toBe(player);
        });

        it('initialize cpu', () => {
            // Arrange
            const player: IPlayer = new Player('', 1, 1, new ElementsStats(0, 0, 0), [], 1, 1);
            const npc: INPC = new NPC('', 0, 1, new ElementsStats(0, 0, 0), [], '');

            bossSpawnerMock = jest.fn<IBossSpawner>().mockImplementation(() => {
                return {
                    // tslint:disable-next-line:arrow-return-shorthand
                    getBoss: jest.fn().mockReturnValue(npc)
                };
            });

            const dom: IDomManipulator = new domMock();
            const combatOutcome: ICombatOutcome = new combatOutcomeMock();
            const npcManager: INPCAbilityManager = new npcManagerMock();
            const bspawn: IBossSpawner = new bossSpawnerMock();

            const combatManager: ICombatManager = new CombatManager(combatOutcome, bspawn, dom, npcManager);

            // Act
            combatManager.init(player);

            // Assert
            expect(combatManager._cpu).toBeDefined();
            expect(combatManager._cpu).toBe(npc);
        });

        it('call all dom manipulation methods', () => {
            // Arrange
            const player: IPlayer = new Player('', 1, 1, new ElementsStats(0, 0, 0), [], 1, 1);

            const dom: IDomManipulator = new domMock();
            const combatOutcome: ICombatOutcome = new combatOutcomeMock();
            const npcManager: INPCAbilityManager = new npcManagerMock();
            const bspawn: IBossSpawner = new bossSpawnerMock();

            const combatManager: ICombatManager = new CombatManager(combatOutcome, bspawn, dom, npcManager);

            const barUpdatorSpy: jest.SpyInstance = jest.spyOn(dom, 'updateAllBars');
            const prepareButtonSpy: jest.SpyInstance = jest.spyOn(dom, 'prepareAbilityButtons');
            const imgChangerSpy: jest.SpyInstance = jest.spyOn(dom, 'changeBossImage');
            const listenerSpy: jest.SpyInstance = jest.spyOn(dom, 'addAbilityListeners');
            const prepareIconSpy: jest.SpyInstance = jest.spyOn(dom, 'prepareItemIcons');
            const startButtonHiderSpy: jest.SpyInstance = jest.spyOn(dom, 'hideStartGameButton');

            // Act
            combatManager.init(player);

            // Assert
            expect(barUpdatorSpy).toHaveBeenCalled();
            expect(prepareButtonSpy).toHaveBeenCalled();
            expect(imgChangerSpy).toHaveBeenCalled();
            expect(listenerSpy).toHaveBeenCalled();
            expect(prepareIconSpy).toHaveBeenCalled();
            expect(startButtonHiderSpy).toHaveBeenCalled();
        });
    });
});
