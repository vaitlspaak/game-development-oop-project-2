import { ItemType } from './../src/common/item-types';
import { IPlayer } from './../src/contracts/models/characters/player';
import { IInventory } from './../src/contracts/models/inventory';
import { IItem } from './../src/contracts/models/items/item';
import { Player } from './../src/models/characters/player-model';
import { ElementsStats } from './../src/models/elements-stats-model';
import { Inventory } from './../src/models/inventory-model';
import { Item } from './../src/models/items/item-model';

describe('Player model leveUp method must:', () => {
  it('give addiional HP & Mana', () => {
    // Arrange
    const player: IPlayer = new Player(
      '',
      1,
      100,
      new ElementsStats(0, 0, 0),
      [],
      100,
      1
    );

    // Act
    player.levelUP();

    // Assert
    expect(player.maxHP).toBe(130);
    expect(player.maxMana).toBe(120);
  });

  it('mana refill must not exceed maxMana when used', () => {
    // Arrange
    const player: IPlayer = new Player(
      '',
      1,
      100,
      new ElementsStats(0, 0, 0),
      [],
      120,
      1
    );

    // Act
    player.mana -= 20;
    player.mana += 70;

    // Assert
    expect(player.mana).toBe(120);
  });

  it('addItemStats must add all item bonuses to players stats', () => {
    // Arrange
    const player: IPlayer = new Player(
      '',
      1,
      100,
      new ElementsStats(0, 0, 0),
      [],
      100,
      1
    );

    // Act
    player.addItemStats(
      new Item(
        'Shouders',
        ItemType.Shouders,
        new ElementsStats(0, 10, 0),
        1,
        0,
        20,
        `Magic Shouders`
      )
    );

    player.addItemStats(
      new Item(
        'Nail',
        ItemType.Nail,
        new ElementsStats(10, 10, 0),
        1,
        20,
        0,
        `Magic Nail`
      )
    );

    // Assert
    expect(player.maxHP).toBe(120);
    expect(player.maxMana).toBe(120);
    expect(player.elementsStats.firePoints).toBe(20);
    expect(player.elementsStats.frostPoints).toBe(10);
  });

  it('removeItemStats must clear all item bonuses', () => {
    // Arrange
    const player: IPlayer = new Player(
      '',
      1,
      100,
      new ElementsStats(0, 0, 0),
      [],
      100,
      1
    );

    const item1: IItem = new Item(
      'Shouders',
      ItemType.Shouders,
      new ElementsStats(0, 10, 0),
      1,
      0,
      20,
      `Magic Shouders`
    );

    const item2: IItem = new Item(
      'Nail',
      ItemType.Nail,
      new ElementsStats(10, 10, 0),
      1,
      20,
      0,
      `Magic Nail`
    );

    // Act
    player.addItemStats(item1);
    player.addItemStats(item2);
    player.removeItemStats(item1);

    // Assert
    expect(player.maxHP).toBe(120);
    expect(player.maxMana).toBe(100);
    expect(player.elementsStats.firePoints).toBe(10);
    expect(player.elementsStats.frostPoints).toBe(10);
  });
});
