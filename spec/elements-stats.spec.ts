import { IElementsStats } from '../src/contracts/models';
import { ElementsStats } from '../src/models/elements-stats-model';

describe('ElementsStats', () => {
    describe('naturePoints setter should', () => {
        it('throw an error if the given value is less than 0', () => {
            // Arrange
            const stats: IElementsStats = new ElementsStats(0, 0, 0);

            // Act and Assert
            expect(() => (stats.naturePoints = -10)).toThrow();
        });
    });
});
