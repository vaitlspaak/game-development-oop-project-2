import { ItemType } from './../src/common/item-types';
import { IInventory } from './../src/contracts/models/inventory';
import { IItem } from './../src/contracts/models/items/item';
import { IStats } from './../src/contracts/models/stats';
import { ElementsStats } from './../src/models/elements-stats-model';
import { Inventory } from './../src/models/inventory-model';
import { Item } from './../src/models/items/item-model';

describe('InventoryMOdel ', () => {
  it('getStatsMethod should get stats from items', () => {
    // Arrange

    const inventory: IInventory = new Inventory();
    inventory.addItem(
      new Item(
        'Nail',
        ItemType.Nail,
        new ElementsStats(10, 10, 0),
        1,
        20,
        0,
        `Magic Nail`
      )
    );

    inventory.addItem(
      new Item(
        'Shouders',
        ItemType.Shouders,
        new ElementsStats(0, 10, 0),
        0,
        0,
        20,
        `Magic Shouders`
      )
    );

    // Act
    const inventoryStats: IStats = inventory.getStats();

    // Assert
    expect(inventoryStats.bonusHp).toBe(20);
    expect(inventoryStats.bonusMana).toBe(20);
    expect(inventoryStats.elementsStats.firePoints).toBe(20);
    expect(inventoryStats.elementsStats.frostPoints).toBe(10);
  });

  it('maxSlots must be at least 12 in order not to lose items', () => {
    // Arrange
    const inventory: IInventory = new Inventory();

    // Act & Assert
    expect(() => (inventory.maxSlots = 8)).toThrowError(
      'Inventory`s MaxSlots must be at least 12'
    );
  });

  it('addItemMethod should not add more items than the current maxSlots available', () => {
    // Arrange
    const inventory: IInventory = new Inventory();
    const item: IItem = new Item(
      'Shouders',
      ItemType.Shouders,
      new ElementsStats(0, 10, 0),
      0,
      0,
      20,
      `Magic Shouders`
    );

    // Act
    inventory.maxSlots = 12;

    for (let i: number = 0; i <= 11; i = i + 1) {
      inventory.addItem(item);
    }
    // Assert
    expect(() => inventory.addItem(item)).toThrowError(
      'Maximum slots capacity exceeded'
    );
  });
});
