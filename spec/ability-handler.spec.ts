import { AbilityHandler } from '../src/ability-handler';
import { IAbility, INPC, IPlayer } from '../src/contracts/models';
import { IDomManipulator } from '../src/contracts/models/DOMManipulation/IDomManipulator';
import { Ability, NPC, Player } from '../src/models';
import { ElementsStats } from '../src/models/elements-stats-model';
import { AbilityType } from './../src/common/ability-types';

describe('AbilityHandler', () => {
    let domMock: jest.Mock<IDomManipulator>;

    beforeEach(() => {
        // tslint:disable-next-line:arrow-return-shorthand
        domMock = jest.fn<IDomManipulator>().mockImplementation(() => {
            return {
                logToConsole: jest.fn(),
                playSound: jest.fn()
            };
        });
    });

    describe('activateAttackAbility method should', () => {
        it('deal damage to opponent', () => {
            // Arrange
            const dom: IDomManipulator = new domMock();
            AbilityHandler.dom = dom;
            const ability: IAbility = new Ability(AbilityType.Attack, 0, 10);
            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
            const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

            // Act
            AbilityHandler.ACTIVATEATTACKABILITY(ability, player, npc);

            // Assert
            expect(npc.hp).toBe(90);
        });

        it('should call logToConsole method', () => {
            // Arrange
            const dom: IDomManipulator = new domMock();
            AbilityHandler.dom = dom;
            const ability: IAbility = new Ability(AbilityType.Attack, 0, 10);
            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
            const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

            const spy: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');

            // Act
            AbilityHandler.ACTIVATEATTACKABILITY(ability, player, npc);

            // Assert
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('activateFireAbility method should', () => {
        it('deal damage to opponent', () => {
            // Arrange
            const dom: IDomManipulator = new domMock();
            AbilityHandler.dom = dom;
            const ability: IAbility = new Ability(AbilityType.Fire, 0, 20);
            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
            const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

            // Act
            AbilityHandler.ACTIVATEFIREABILITY(ability, player, npc);

            // Assert
            expect(npc.hp).toBe(80);
        });

        it('apply relevant fire damage from bonus items', () => {
            // Arrange
            const dom: IDomManipulator = new domMock();
            AbilityHandler.dom = dom;
            const ability: IAbility = new Ability(AbilityType.Fire, 0, 20);
            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 10, 0), [ability], 100, 1);
            const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

            // Act
            AbilityHandler.ACTIVATEFIREABILITY(ability, player, npc);

            // Assert
            expect(npc.hp).toBe(70);
        });

        it('should call logToConsole method', () => {
            // Arrange
            const dom: IDomManipulator = new domMock();
            AbilityHandler.dom = dom;
            const ability: IAbility = new Ability(AbilityType.Fire, 0, 20);
            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
            const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

            const spy: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');

            // Act
            AbilityHandler.ACTIVATEFIREABILITY(ability, player, npc);

            // Assert
            expect(spy).toHaveBeenCalled();
        });

        describe('activateFrostAbility method should', () => {
            it('deal damage to opponent', () => {
                // Arrange
                const dom: IDomManipulator = new domMock();
                AbilityHandler.dom = dom;
                const ability: IAbility = new Ability(AbilityType.Frost, 0, 30);
                const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

                // Act
                AbilityHandler.ACTIVATEFROSTABILITY(ability, player, npc);

                // Assert
                expect(npc.hp).toBe(70);
            });

            it('apply relevant frost damage from bonus items', () => {
                // Arrange
                const dom: IDomManipulator = new domMock();
                AbilityHandler.dom = dom;
                const ability: IAbility = new Ability(AbilityType.Frost, 0, 30);
                const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(10, 0, 0), [ability], 100, 1);
                const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

                // Act
                AbilityHandler.ACTIVATEFROSTABILITY(ability, player, npc);

                // Assert
                expect(npc.hp).toBe(60);
            });

            it('should call logToConsole method', () => {
                // Arrange
                const dom: IDomManipulator = new domMock();
                AbilityHandler.dom = dom;
                const ability: IAbility = new Ability(AbilityType.Frost, 0, 30);
                const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

                const spy: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');

                // Act
                AbilityHandler.ACTIVATEFROSTABILITY(ability, player, npc);

                // Assert
                expect(spy).toHaveBeenCalled();
            });

            describe('activateNatureAbility method should', () => {
                it('deal damage to opponent', () => {
                    // Arrange
                    const dom: IDomManipulator = new domMock();
                    AbilityHandler.dom = dom;
                    const ability: IAbility = new Ability(AbilityType.Nature, 0, 40);
                    const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                    const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

                    // Act
                    AbilityHandler.ACTIVATENATUREABILITY(ability, player, npc);

                    // Assert
                    expect(npc.hp).toBe(60);
                });

                it('apply relevant nature damage from bonus items', () => {
                    // Arrange
                    const dom: IDomManipulator = new domMock();
                    AbilityHandler.dom = dom;
                    const ability: IAbility = new Ability(AbilityType.Nature, 0, 40);
                    const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 10), [ability], 100, 1);
                    const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

                    // Act
                    AbilityHandler.ACTIVATENATUREABILITY(ability, player, npc);

                    // Assert
                    expect(npc.hp).toBe(50);
                });

                it('should call logToConsole method', () => {
                    // Arrange
                    const dom: IDomManipulator = new domMock();
                    AbilityHandler.dom = dom;
                    const ability: IAbility = new Ability(AbilityType.Nature, 0, 40);
                    const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                    const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');
                    const spy: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');
                    // Act
                    AbilityHandler.ACTIVATENATUREABILITY(ability, player, npc);
                    // Assert
                    expect(spy).toHaveBeenCalled();
                });

                describe('activateHealingAbility method should', () => {
                    it('heal 40% of maxHp on player', () => {
                        // Arrange
                        const dom: IDomManipulator = new domMock();
                        AbilityHandler.dom = dom;
                        const ability: IAbility = new Ability(AbilityType.Healing, 0, 40);
                        const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                        // Act
                        player.hp = 60;
                        AbilityHandler.ACTIVATEHEALINGABILITY(ability, player);
                        // Assert
                        expect(player.hp).toBe(100);
                    });

                    it('should call logToConsole method', () => {
                        // Arrange
                        const dom: IDomManipulator = new domMock();
                        AbilityHandler.dom = dom;
                        const ability: IAbility = new Ability(AbilityType.Healing, 0, 40);
                        const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                        const spy: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');
                        // Act
                        AbilityHandler.ACTIVATEHEALINGABILITY(ability, player);
                        // Assert
                        expect(spy).toHaveBeenCalled();
                    });

                    describe('activateManaAbility method should', () => {
                        it('give 40% of maxMana on player', () => {
                            // Arrange
                            const dom: IDomManipulator = new domMock();
                            AbilityHandler.dom = dom;
                            const ability: IAbility = new Ability(AbilityType.Mana, 0, 10);
                            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                            // Act
                            player.mana = 60;
                            AbilityHandler.ACTIVATEMANAABILITY(ability, player);
                            // Assert
                            expect(player.hp).toBe(100);
                        });

                        it('should call logToConsole method', () => {
                            // Arrange
                            const dom: IDomManipulator = new domMock();
                            AbilityHandler.dom = dom;
                            const ability: IAbility = new Ability(AbilityType.Mana, 0, 40);
                            const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                            const spy: jest.SpyInstance = jest.spyOn(dom, 'logToConsole');
                            // Act
                            AbilityHandler.ACTIVATEMANAABILITY(ability, player);
                            // Assert
                            expect(spy).toHaveBeenCalled();
                        });

                        describe('handleAbility method should', () => {
                            it('call the right activation method', () => {
                                // Arrange
                                const dom: IDomManipulator = new domMock();
                                AbilityHandler.dom = dom;
                                const ability: IAbility = new Ability(AbilityType.Frost, 0, 10);
                                const player: IPlayer = new Player('Pencho', 1, 100, new ElementsStats(0, 0, 0), [ability], 100, 1);
                                const npc: INPC = new NPC('Omraz', 1, 100, new ElementsStats(0, 0, 0), [], '');

                                const spyAttack: jest.SpyInstance = jest.spyOn(AbilityHandler, 'ACTIVATEATTACKABILITY');
                                const spyFrost: jest.SpyInstance = jest.spyOn(AbilityHandler, 'ACTIVATEFROSTABILITY');
                                const spyNature: jest.SpyInstance = jest.spyOn(AbilityHandler, 'ACTIVATENATUREABILITY');
                                const spyFire: jest.SpyInstance = jest.spyOn(AbilityHandler, 'ACTIVATEFIREABILITY');
                                const spyHealing: jest.SpyInstance = jest.spyOn(AbilityHandler, 'ACTIVATEHEALINGABILITY');
                                const spyMana: jest.SpyInstance = jest.spyOn(AbilityHandler, 'ACTIVATEMANAABILITY');

                                // Act
                                AbilityHandler.HANDLEABILITY(ability, player, npc);

                                // Assert
                                expect(spyFrost).toHaveBeenCalled();
                                expect(spyAttack).not.toHaveBeenCalled();
                                expect(spyNature).not.toHaveBeenCalled();
                                expect(spyFire).not.toHaveBeenCalled();
                                expect(spyHealing).not.toHaveBeenCalled();
                                expect(spyMana).not.toHaveBeenCalled();
                            });
                        });
                    });
                });
            });
        });
    });
});
