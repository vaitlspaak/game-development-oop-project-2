export interface ITypes {
  IEngine: symbol;
  IInventory: symbol;
  IBossSpawner: symbol;
  ICombatOutcome: symbol;
  IPlayerStatsUpdater: symbol;
  ILocalStorageDatabaseManipulator: symbol;
  IDomManipulator: symbol;
  ISetAbilityTitles: symbol;
  INPCAbilityManager: symbol;
  ICombatManager: symbol;
}
