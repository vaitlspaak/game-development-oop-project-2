import { IPlayer } from './../models/characters/player';
export interface IEngine {
  player: IPlayer;
  start(): void;
}
