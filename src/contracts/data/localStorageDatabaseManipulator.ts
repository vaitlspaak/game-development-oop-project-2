import { IPlayer } from './../models/characters/player';
import { IInventory } from './../models/inventory';
import { IItem } from './../models/items/item';
export interface ILocalStorageDatabaseManipulator {
  getSavedPlayerData(): IPlayer | null;
  savePlayerData(player: IPlayer): void;
  savePlayersPassedLevels(level: number): void;
  getPlayersPassedLevels(): number;
  savePlayerInventory(inventory: IItem[]): void;
  getPlayerInventory(): IInventory;
}
