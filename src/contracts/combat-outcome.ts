import { INPC, IPlayer } from './models';
export interface ICombatOutcome {
  onWin(player: IPlayer, cpu: INPC): void;
  onDefeat(player: IPlayer, cpu: INPC): void;
  getOrUpdateItem(player: IPlayer, cpu: INPC): void;
}
