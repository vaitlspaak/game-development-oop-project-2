import { INPC, IPlayer } from './models';

export interface ICombatManager {
  _player: IPlayer;
  _cpu: INPC;
  playerTurn(): Promise<number>;
  cpuTurn(): Promise<number>;
  init(f1: IPlayer): void;
  startFight(): Promise<void>;
}
