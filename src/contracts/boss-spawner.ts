import { INPC } from './models/characters/npc';
export interface IBossSpawner {
  getBoss(): INPC;
}
