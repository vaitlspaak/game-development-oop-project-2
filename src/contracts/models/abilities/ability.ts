import { AbilityType } from '../../../common';
import { ICharacter } from '../characters';

export interface IAbility {
  abilityType: AbilityType;
  manaCost: number;
  effectPoints: number;
  activate(activator: ICharacter, opponent: ICharacter): void;
}
