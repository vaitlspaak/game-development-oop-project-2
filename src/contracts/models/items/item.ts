import { ItemType } from '../../../common';
import { IElementsStats } from '../elements-stats';

export interface IItem {
  title: string;
  level: number;
  itemType: ItemType;
  elementsStats: IElementsStats;
  bonusMana: number;
  bonusHp: number;
  itemDesciption: string;
  upgrade(): void;
}
