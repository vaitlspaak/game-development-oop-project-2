import { IElementsStats } from './elements-stats';

export interface IStats {
    bonusHp: number;
    bonusMana: number;
    elementsStats: IElementsStats;
}
