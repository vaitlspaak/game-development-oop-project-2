import { ICharacter, IPlayer } from '..';
import { INPC } from '../characters';
import { AbilitySound } from './../../../common/ability-sounds';
import { AbilityType } from './../../../common/ability-types';
import { itemDatabase } from './../../../data/item-database';
import { IAbility } from './../abilities/ability';
import { IItem } from './../items/item';
export interface IDomManipulator {
  updateBossHealthBar(npc: ICharacter): void;
  updatePlayerHealthBar(player: ICharacter): void;
  updatePlayerManaBar(player: ICharacter): void;
  changeBossImage(imagePath: string): void;
  logToConsole(textToPrint: string, idToAdd: string): void;
  prepareAbilityButtons(abilities: IAbility[], character: string): void;
  addAbilityListeners(
    abilities: IAbility[],
    player: IPlayer,
    opponent: ICharacter
  ): void;
  playSound(soundID: string): void;
  checkForManaSufficiency(player: IPlayer): void;
  updateAllBars(player: ICharacter, npc: ICharacter): void;
  prepareItemIcons(itemDatabase: IItem[]): void;
  setAbilityTitles(player: IPlayer): void;
  hideStartGameButton(): void;
  showStartGameButton(): void;
  applyTurnEffect(fighter: string): void;
  showLoadButton(): void;
  hideLoadButton(): void;
}
