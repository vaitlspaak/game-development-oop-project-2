export interface IElementsStats {
  frostPoints: number;
  firePoints: number;
  naturePoints: number;
}
