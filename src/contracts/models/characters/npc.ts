import { ICharacter } from '.';
import { IItem } from '../items';

export interface INPC extends ICharacter {
  _imagePath: string;
  giveItem(): IItem;
}
