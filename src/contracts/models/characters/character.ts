import { IAbility, IElementsStats } from '..';

export interface ICharacter {
  name: string;
  level: number;
  hp: number;
  maxHP: number;
  elementsStats: IElementsStats;
  abilities: IAbility[];
}
