import { ICharacter } from '.';
import { IInventory, IItem } from '../';
import { IAbility } from '../abilities';

export interface IPlayer extends ICharacter {
  experience: number;
  inventory: IInventory;
  mana: number;
  maxMana: number;
  abilities: IAbility[];
  levelUP(): void;
  addItemStats(item: IItem): void;
  removeItemStats(item: IItem): void;
}
