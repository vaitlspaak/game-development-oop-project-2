import { IItem } from './items';
import { IStats } from './stats';

export interface IInventory {
  maxSlots: number;
  items: IItem[];
  addItem(item: IItem): IItem;
  getStats(): IStats;
}
