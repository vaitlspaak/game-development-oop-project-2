import { ICharacter } from './models';
import { IAbility } from './models/abilities/ability';

export interface INPCAbilityManager {
  performRandomAbility(npc: ICharacter, opponent: ICharacter): IAbility;
}
