export interface IProgressBarIDs {
  bossHealtBarId: string;
  playerHealtBarId: string;
  playerManaBarId: string;
  playerExperienceId: string;
}
