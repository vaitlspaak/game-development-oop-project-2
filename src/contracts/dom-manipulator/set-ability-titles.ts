import { IPlayer } from './../models/characters/player';
export interface ISetAbilityTitles {
  setAbilityTitles(player: IPlayer): void;
}
