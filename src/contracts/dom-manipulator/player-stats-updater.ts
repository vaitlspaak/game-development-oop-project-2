import { IPlayer } from '../models';

export interface IPlayerStatsUpdater {
  playerStatsUpdater(player: IPlayer): void;
}
