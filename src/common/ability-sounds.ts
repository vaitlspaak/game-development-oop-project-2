export enum AbilitySound {
    AttackSound,
    FireSound,
    FrostSound,
    NatureSound,
    HealingSound,
    ManaSound
  }
