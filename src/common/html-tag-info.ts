import { IProgressBarIDs } from './../contracts/common/html-tag-info';
// tslint:disable-next-line:no-any
export const progressBarIDs: IProgressBarIDs = {
  bossHealtBarId: 'bossHealth',
  playerHealtBarId: 'playerHealth',
  playerManaBarId: 'playerMana',
  playerExperienceId: 'playerXP'
};
