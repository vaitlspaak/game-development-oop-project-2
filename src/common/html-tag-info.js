"use strict";
exports.__esModule = true;
// tslint:disable-next-line:no-any
exports.progressBarIDs = {
    bossHealtBarId: 'bossHealth',
    playerHealtBarId: 'playerHealth',
    playerManaBarId: 'playerMana',
    playerExperienceId: 'playerXP'
};
