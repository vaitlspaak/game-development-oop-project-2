export enum AbilityType {
  Attack,
  Fire,
  Frost,
  Nature,
  Healing,
  Mana
}
