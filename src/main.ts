import { AbilityHandler } from './ability-handler';
import { container } from './config/ioc.config';
import { TYPES } from './config/type';
import { IEngine } from './contracts/engine/engine';

AbilityHandler.INIT(container);

const engine: IEngine = container.get<IEngine>(TYPES.IEngine);
engine.start();
